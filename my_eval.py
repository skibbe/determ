#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 16 13:35:33 2018

@author: skibbe-h
"""
from __future__ import print_function

import signal
import numpy as np
import tensorflow as tf
import scipy
import sys
import os
import skimage
import skimage.morphology as morphology
import argparse
from PIL import Image
import csv
import h5py
import matplotlib.pyplot as plt
from packaging import version

def dsp(txt):
    print(txt)
    sys.stdout.flush()
    
#%%
def get_detection(rimg, **kwargs):
    print("##############################")
    params = {'offset':[0,0],'shape':None,'mask':None,'smooth':1.5,'threshold':0.5,'quiet':False,'bb':None}
        
    if kwargs is not None:
        for key, value in kwargs.items():
            params[key]=value;
            
    quiet = params['quiet']
    img_offset = params['offset']    
    shape = params['shape']    
    bb = params['bb']
    
    if shape is None:
        shape = rimg.shape
        
    #scores = rimg.copy()
    scores = (rimg[img_offset[0]: img_offset[0] + shape[0],
                img_offset[1]: img_offset[1] + shape[1]]).copy()
        
    
    if params['threshold'] > 0:
        rimg = (rimg > params['threshold']).astype(float)
    

            
    if params['smooth'] > 0:
        # we first smooth the detections a bit
        rimg = skimage.filters.gaussian(
            rimg, sigma=params['smooth'], mode='nearest', truncate=round(4*params['smooth']+0.5))
            #rimg, sigma=3.5, mode='nearest', truncate=6.0)

    
    # then we extract local maxima
    if version.parse(skimage.__version__)<= version.parse("0.13.1"):
        #original behaviour
        struct1 = scipy.ndimage.iterate_structure(
            scipy.ndimage.generate_binary_structure(2, 1), 3)
    else:
        #new behaviour
        struct1 = scipy.ndimage.iterate_structure(
            scipy.ndimage.generate_binary_structure(2, 1), 1)
        print("############################################################")
        print("warning: with scikit-image=0.13.1, the deteced terminals may ")
        print("slightly differ from the original implementation of DeTerm")
        print("############################################################")
        
    
    #rimg_mask = morphology.local_maxima(rimg, selem=struct1)
    try:
        rimg_mask = morphology.local_maxima(rimg, selem=struct1)
        rimg[np.logical_not(rimg_mask)] = 0;
    except Exception as e:
        #print(format(e))
        rimg[:] = 0;
    
    #rimg[np.logical_not(rimg_mask)] = 0;

    
    # we have temporarily increase the image size,
    # now we crop the image to its original dimension

   
    
    rimg = rimg[img_offset[0]: img_offset[0] + shape[0],
                img_offset[1]: img_offset[1] + shape[1]]
    #img_fg = img_fg[img_offset[0]: img_offset[0] + shape[0],
    #                img_offset[1]: img_offset[1] + shape[1]]

    if params['mask'] is not None:
        if not quiet:
            dsp(format(params['mask'].shape))
        rimg = rimg * params['mask']

    # due to the discretization, a maxima may consist of more than one pixel.
    # --> we only keep the avg position
    rimgl, num_labels = scipy.ndimage.measurements.label(
        rimg)
    labeld_areas = scipy.ndimage.find_objects(
        rimgl, num_labels)
    #if not quiet:
    #    dsp("total num detections: "+str(num_labels))
    
   
    #print("MIN {} MAX {} {}".format(np.min(rimg), np.max(rimg),rimg.dtype))
    rimg[:] = 0
    #print("MIN {} MAX {} {}".format(np.min(rimg), np.max(rimg),rimg.dtype))
    term_pos = []
    for roi in labeld_areas:
        pos = np.array([0.0, 0.0, 0.0])
        # dsp("")
        pos[0:2] += np.array([roi[0].start, roi[1].start])
        pos[0:2] += np.array([roi[0].stop, roi[1].stop])
        pos[0:2] /= 2
        pos[0:2] = np.maximum(pos[0:2], np.array([0, 0]))
        pos[0:2] = np.minimum(pos[0:2], np.array(rimg.shape)-1)
        
        pos_i = np.round(pos[0:2]).astype(int)
        
        #pos[2] = scores[pos_i[0],pos_i[1]]
        pos[2] = np.max(scores[roi[0].start:roi[0].stop,roi[1].start:roi[1].stop])
        
        in_bb = lambda x: False if x[0]<bb[0] or x[0]>bb[1] or x[1]<bb[2] or x[1]>bb[3] else True
       # print(bb)
        if bb is None or in_bb(pos):

            
            
            rimg[pos_i[0], pos_i[1]] = pos[2]
            
            #print("{} {}".format(scores[pos_i[0],pos_i[1]],pos))
            #print("{}".format(rimg[pos_i[0], pos_i[1]]))
            
            #print("{} {}".format(scores[pos_i[0],pos_i[1]],pos))
            #if scores[pos_i[0],pos_i[1]]>0.1:
            term_pos.append(pos)
            # dsp(str(pos_i))
    
    if not quiet:
        dsp("total num detections: "+str(len(term_pos)))

    if len(term_pos) > 0:
        term_pos_array = np.reshape(np.concatenate(term_pos),(len(term_pos),3))    
    
        #print("BLA BLA {}".format(term_pos_array[:,0:2]))
        #print("BLA BLA {}".format(term_pos_array.shape))
        #print("MIN {} MAX {} {}".format(np.min(rimg), np.max(rimg),rimg.dtype))
        return term_pos_array, rimg, term_pos    
    else:
        return None, rimg, term_pos    

def distmat(a, b):
    dmat = ((-2*np.matmul(a,np.transpose(b)))) \
    + np.matlib.repmat(np.reshape(np.sum(a**2,1),(a.shape[0],1)),1,b.shape[0])  \
    + np.transpose(np.matlib.repmat(np.reshape(np.sum(b**2,1),(b.shape[0],1)),1,a.shape[0]))
    dmat[dmat<0] = 0
    return dmat


def detection_performance(img_result,img_gt, smooth=1.5, radius=2,quiet=False):
    
    #print(str(quiet))
    #print("gt shape {}".format(img_gt.shape))
    #print("det shape {}".format(img_result.shape))
    #print("det rad: {}, smooth {}".format(radius,smooth))
    try:
        #dsp("detecting in results")
        detection_pos, detection_img ,_ = get_detection(img_result,smooth=smooth,quiet=quiet)
        #dsp("detecting in gt")
        detection_gt_pos, detection_gt_img,_ = get_detection(img_gt,smooth=0,quiet=quiet)
        #dsp("evaluating")
        
        no_detections = ( detection_pos is None )
        no_gt = ( detection_gt_pos is None )
        
        if (no_detections and no_gt) :
            return 1, 0
        
        if (no_detections and not no_gt) :
            return 0, 0
        
        if (not no_detections and no_gt) :
            return 0, 1
        
        
        DM = distmat(detection_pos[:,0:2],detection_gt_pos[:,0:2])
        gt2det_mindist = DM.min(axis=0);
        det2gt_mindist = DM.min(axis=1);
        
        TP = np.sum( gt2det_mindist < radius**2 ) / detection_gt_pos.shape[0]
        FP = np.sum( det2gt_mindist > radius**2 ) / detection_pos.shape[0]
    except Exception as e:
        if not quiet:
            dsp("evaluation failed: "+str(e))
        TP = 0
        FP = 1
    return TP, FP