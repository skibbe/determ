from __future__ import print_function


import signal
import numpy as np
import scipy
import scipy.io
import sys


def dsp(txt="",o=False):
    if o:
        #sys.stdout.write('%s\r' % txt)
        #sys.stdout.flush()
        #print(txt, end='\r')
        print("'\r{0}".format(txt), end='')
    else:
        print(txt)
    sys.stdout.flush()
    
