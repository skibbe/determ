from __future__ import print_function

import sys
import csv
from roifile import ImagejRoi,ROI_TYPE
import argparse

def dsp(txt):
    print(txt)
    sys.stdout.flush()

parser = argparse.ArgumentParser()


parser.add_argument("--csv", default="",
                    help="output terminal postions",required=True)

parser.add_argument("--roi", default="",
                    help="detected terminal postions as imageJ roi file",required=True)
                    

args = parser.parse_args()

roi = ImagejRoi.fromfile(args.roi)

with open(args.csv, 'w') as csvfile:
    dsp("writing results into {} ".format(args.csv))
    fieldnames = ['', 'Area', 'Mean',
                    'Min', 'Max', 'X', 'Y']
    writer = csv.DictWriter(
        csvfile, fieldnames=fieldnames)
    writer.writeheader()
    count = 1
    for p in roi.subpixel_coordinates:
            writer.writerow(
                {'': str(count), 'X': str(p[0]), 'Y': str(p[1])})
            count += 1
