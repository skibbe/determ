class tracer_cnn:

    @staticmethod
    def get_bottom_out(lowest,steps,feature_layers):
        print("IN {}".format(lowest-feature_layers*2))
        if steps>1:
            return tracer_cnn.get_bottom_out((lowest-feature_layers*2)/2,steps-1,feature_layers)
        else:
            return (lowest-feature_layers*2)
        
    @staticmethod
    def get_input_shape(lowest,steps,feature_layers):
        print("IN {}".format(lowest+feature_layers*2))
        if steps>1:
            return tracer_cnn.get_input_shape((lowest+feature_layers*2)*2,steps-1,feature_layers)
        else:
            return (lowest+feature_layers*2)
    @staticmethod
    def get_output_shape(shape,steps,feature_layers,mode=0):
        if mode==0:
            shape = tracer_cnn.get_bottom_out(shape,steps,feature_layers)
        print("PREDICTION SHAPE {}".format(shape))
        if steps>2:
            return tracer_cnn.get_output_shape((shape*2-2*feature_layers),steps-1,feature_layers,1)
        else:
            return (shape*2-2*feature_layers)   

    @staticmethod
    def avg_pool():
        pass
