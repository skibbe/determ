#   this software is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.


#python detect_terminals.py --fg ../uemura-sensei/2018_corrected_terminals/Fused_180130_ppkGal4_UAS-mCD8GFP_TrpA1K_8Y_4_A3m/Fused_180130_ppkGal4_UAS-mCD8GFP_TrpA1K_8Y_#4_A3m_159-234.tif  --cout output/myout.jpg --model ./model/ --prob 0.001 --dout output/dmyout.jpg  --csv output/my.csv

from __future__ import print_function

import signal
import numpy as np
#import tensorflow as tf
import tensorflow.compat.v1  as tf
import scipy.misc
import scipy
import sys
import os
import skimage
import skimage.morphology as morphology
import argparse
#import imageio
from PIL import Image
import scipy.misc
import csv
from my_eval import *
import imp
from roifile import ImagejRoi,ROI_TYPE

#tf.disable_v2_behavior()



scale_intensity = 1.0 / 256
output_shape = [572, 572]


def normalize(img):
    img -= np.mean(img)
    img /= np.std(img)+0.00000000001
    return img


def dsp(txt):
    print(txt)
    sys.stdout.flush()


global terminate
terminate = False


def signal_handler(signal, frame):
    dsp('terminating program')
    global terminate
    terminate = True

def rgb2gray(rgb):
    if len(rgb.shape) == 3:
        r, g, b = rgb[:,:,0], rgb[:,:,1], rgb[:,:,2]
        return 0.2989 * r + 0.5870 * g + 0.1140 * b
    return rgb

def rgb2gray2(rgb):
    if len(rgb.shape) == 3:
        return np.max(rgb,axis=2)
    return rgb

signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)

parser = argparse.ArgumentParser()

parser.add_argument(
    "--fg", help="input image (if RGB, it will be converted to an intensity image)")
parser.add_argument("--invert", action='store_true', help="invert intensities")
parser.add_argument("--guess", action='store_true',
                    help="guess which intensity is FG and which is BG (auto invert)")

parser.add_argument("--prob", default=0.5, type=float,
                    help="probability threshold")


parser.add_argument("--scale", default=1.0, type=float,
                    help="multiply position of detected terminal by scale")


parser.add_argument("--out", default="",
                    help="a binary image with the detected locations")

parser.add_argument("--dout", default="",
                    help="debug image")

parser.add_argument("--cout", default="",
                    help="an image with the detected locations in red, the input image in green, a mask appears in blue")
parser.add_argument("--cout_dark", action='store_true',
                    help="with black background")

parser.add_argument("--csv", default="",
                    help="detected terminal postions as csv table")

parser.add_argument("--roi", default="",
                    help="detected terminal postions as imageJ roi file")


parser.add_argument("--model", default="./model/",
                    help="network model directory")

parser.add_argument("--chkpt", default="newest",
                    help="model checkpoint")

parser.add_argument("--mask", default=None,
                    help="RGB img, where the RED channel is used as mask for the terminals")


parser.add_argument("--mask_alpha", default=0.2, type=float,
                    help="mask alpha")


parser.add_argument("--terminal_alpha", default=0.5, type=float,
                    help="terminal alpha")


parser.add_argument("--terminal_size", default=2, type=int,
                    help="terminal_size")


parser.add_argument(
    "--myout", help="customized result image ",nargs='+', type=str, required=False)

parser.add_argument("--colorize", default="",type=str,
                    help="R,G,B/R,G,B/R,G,B (img,label,mask)")

                    

args = parser.parse_args()


mycolors = None
if len(args.colorize)>0:
    mycolors = [[0,0,0],[0,0,0],[0,0,0]]
    colors = args.colorize.split("/")
    count = 0
    for col in colors:
        c = col.split(",")
        mycolors[count][0] = float(c[0])
        mycolors[count][1] = float(c[1])
        mycolors[count][2] = float(c[2])
        count+=1


#model_dir = args.model
#model_name = model_dir+"terminal_det_net"

model_name = args.model
model_dir =  os.path.dirname(model_name)+"/"

print("using network params from "+model_dir)
module = imp.load_source('net_params', model_dir+'/net_params.py')
params = module.set_params()
dsp = module.dsp

dsp("")
dsp("")
dsp("this software is distributed in the hope that it will be useful,")
dsp("but WITHOUT ANY WARRANTY; without even the implied warranty of")
dsp("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.")
dsp("")
dsp("")



if True:
    dsp("reading the image")
    #img_fg_ = np.array(scipy.misc.imread(args.fg, mode='F')).astype(float)
    img_fg_ = rgb2gray(np.array(Image.open(args.fg)).astype(float))
    #img_fg_ = rgb2gray(np.array(imageio.imread(args.fg)).astype(float))
    #from PIL import Image
    dsp("image shape {}".format(img_fg_.shape))
    dsp("image min max {} {}".format(np.min(img_fg_),np.max(img_fg_)))

    # check for inverted intensities (neurons should be bright)
    intensity_invert = False
    if args.guess:
        t = np.mean(img_fg_)
        total_n_pixel = img_fg_.shape[0] * img_fg_.shape[1]
        bright_n_pixel = np.sum(img_fg_ > t)
        if 2*bright_n_pixel > total_n_pixel:
            intensity_invert = True

    if args.invert or intensity_invert:
        dsp("inverting the image")
        img_fg_ = np.max(img_fg_) - img_fg_

    img_fg_ *= scale_intensity
    # normalize the input image
    #dsp("intensity range before normalizing the image: [{} {}]".format(
    #    np.min(img_fg_), np.max(img_fg_)))
    #img_fg_ = normalize(
    #    img_fg_ * scale_intensity)
    #dsp("intensity range after normalizing the image: [{} {}]".format(
    #    np.min(img_fg_), np.max(img_fg_)))

    # pad the image with zero borders
    img_shape = np.maximum(img_fg_.shape, output_shape) + \
        (output_shape - np.asarray([452, 452]))

    img_shape += img_shape % 2
    print("temporary image shape  {}".format(img_shape))
    img_fg = np.zeros(img_shape, dtype=img_fg_.dtype)

    img_offset = (((img_shape) - (img_fg_.shape))/2).astype(int)
    print("offset {}".format(img_offset))
    img_fg[img_offset[0]: img_offset[0] + img_fg_.shape[0],
           img_offset[1]: img_offset[1] + img_fg_.shape[1]] = img_fg_

    dsp("preparing NN")
    full_shape = img_fg.shape
    new_graph = tf.Graph()
    with tf.compat.v1.Session(graph=new_graph) as sess:
        # First let's load meta graph and restore weights
        saver = tf.compat.v1.train.import_meta_graph(model_name+'-0.meta')

        graph = tf.compat.v1.get_default_graph()

        # x is our input image, y the prediction
        y = graph.get_tensor_by_name("loss_layer"+str(0)+"ypred:0")
        x = graph.get_tensor_by_name("x:0")


        dropout_up = graph.get_tensor_by_name("dropout_up:0")
        dropout_down = graph.get_tensor_by_name("dropout_down:0")
        is_training = graph.get_tensor_by_name("is_training:0")

        sess.run(tf.compat.v1.global_variables_initializer())

        # define the model of the NN
        if args.chkpt == "newest":
            chkpt = tf.train.latest_checkpoint(
                model_dir, latest_filename="checkpoint")
            dsp("using most recent checkpoint {}".format(chkpt))
        else:
            chkpt = model_name + "-" + args.chkpt
            dsp("using the recent checkpoint {}".format(chkpt))

        if chkpt is not None:
            # load and restore the model
            saver.restore(sess, chkpt)
            img = img_fg[np.newaxis, :, :, np.newaxis]

            # we cannot process the image at once
            # --> we work on smaller image tiles of size "output_shape"
            patch_size = np.array([output_shape[0], output_shape[1]])
            output_size = None
            shape_offset = None
            offset = [0, 0]

            # in rimg we store the results (predictions of the NN)
            rimg = np.zeros(full_shape)
            
            if len(args.dout) > 0:
                rimg_raw = np.zeros(full_shape)

            count = 0
            stop_x = 0
            # here we run the image and search for terminals
            while (offset[0] + patch_size[0]-1) < full_shape[0] and (stop_x < 2) and not terminate:

                stop_y = 0
                while (offset[1] + patch_size[1]-1) < full_shape[1] and (stop_y < 2) and not terminate:
                    dsp("predicting {} {}".format(offset[0], offset[1]))
                    count += 1
                    current_img = img[:, offset[0]: offset[0] + patch_size[0],
                                      offset[1]: offset[1] + patch_size[1], :]

                    feed_dict = {x: current_img,
                                 dropout_down: 1,
                                 dropout_up: 1,
                                 is_training: False}
                    prediction = sess.run([y], feed_dict)

                    if output_size is None:
                        output_size = np.array(prediction[0].shape[1:3])
                        shape_offset = (patch_size - output_size)//2

                    rimg[offset[0] + shape_offset[0]: offset[0] + shape_offset[0] + output_size[0],
                          offset[1] + shape_offset[1]: offset[1] + shape_offset[1] + output_size[1]] = (prediction[0][0,:,:, 0]).astype(float)
                         #offset[1] + shape_offset[1]: offset[1] + shape_offset[1] + output_size[1]] = (prediction[0][0, :, :, 0] >= args.prob).astype(float)
                    
                    #if len(args.dout) > 0:
                    #    rimg_raw[offset[0] + shape_offset[0]: offset[0] + shape_offset[0] + output_size[0],
                    #         offset[1] + shape_offset[1]: offset[1] + shape_offset[1] + output_size[1]] = (prediction[0][0, :, :, 0]).astype(float)

                    offset[1] = offset[1] + output_size[1]

                    if (offset[1] + patch_size[1]) > full_shape[1] and (offset[1] + patch_size[1]-1) < full_shape[1]+patch_size[1]:
                        offset[1] = full_shape[1] - patch_size[1]
                        stop_y += 1

                offset[1] = 0
                offset[0] = offset[0] + output_size[0]

                if (offset[0] + patch_size[0]) > full_shape[0] and (offset[0] + patch_size[0]-1) < full_shape[0]+patch_size[0]:
                    offset[0] = full_shape[0] - patch_size[0]
                    stop_x += 1

            # now we determine the locations of the predictions
            if not terminate:
                
              
                
                rimg_raw = rimg
                #eval_Mask = np.squeeze(eval_mask[0][:,:,0]) > 0
                #eval_pred = (eval_Mask*(np.squeeze(pred[0][:,:,0])))>=0.5
                #eval_gt = (eval_Mask*(np.squeeze(GT[0][:,:,0])))>0.5
                #TP_t, FP_t = detection_performance(eval_pred,eval_gt,radius=5,smooth=3.5)
                
                img_mask = None
                if args.mask is not None:
                    dsp("reading the mask")
                    #img_mask_ = np.array(scipy.misc.imread(
                    #    args.mask, mode='RGB')).astype(float)
                    img_mask_ = np.array(Image.open(
                        args.mask)).astype(float)
                    
                    dsp(format(img_mask_.shape))
                    if len(img_mask_.shape)==3:
                        img_mask_ = (img_mask_[:, :, 0] > img_mask_[:, :, 1]) & (
                            img_mask_[:, :, 0] > img_mask_[:, :, 2])
                    else:
                         img_mask_ > 0            
                    print("temporary image shape  {}".format(img_shape))
                    img_mask = np.zeros(img_shape, dtype=img_fg_.dtype)
                
                    img_mask[img_offset[0]: img_offset[0] + img_fg_.shape[0],
                           img_offset[1]: img_offset[1] + img_fg_.shape[1]] = img_mask_
                    #rimg = rimg * img_mask
                    
                #valid = np.zeros(rimg.shape)
                #valid[img_offset[0]+5: img_offset[0] + img_fg_.shape[0]-5,
                #            img_offset[1]+5: img_offset[1] + img_fg_.shape[1]-5] = 1
                #rimg[valid<1] = 0            
                #_, rimg, term_pos = get_detection(rimg,radius=5, smooth=3.5, mask=img_mask)
                #_, rimg, term_pos = get_detection(rimg, smooth=-1, mask=img_mask)
                _, rimg, term_pos = get_detection(rimg, smooth=-1, mask=img_mask, threshold=args.prob,
                bb=[img_offset[0]+5,img_offset[0] + img_fg_.shape[0]-5,img_offset[1]+5,img_offset[1] + img_fg_.shape[1]-5])
                
                # we have temporarily increase the image size,
                # now we crop the image to its original dimension
                #rimg = rimg[img_offset[0]: img_offset[0] + img_fg_.shape[0],
                #            img_offset[1]: img_offset[1] + img_fg_.shape[1]]  >= args.prob
                rimg = rimg[img_offset[0]: img_offset[0] + img_fg_.shape[0],
                            img_offset[1]: img_offset[1] + img_fg_.shape[1]]  > 0
                img_fg = img_fg[img_offset[0]: img_offset[0] + img_fg_.shape[0],
                                img_offset[1]: img_offset[1] + img_fg_.shape[1]]
                
                
                if args.mask is not None:
                    img_mask = img_mask[img_offset[0]: img_offset[0] + img_fg_.shape[0],
                                img_offset[1]: img_offset[1] + img_fg_.shape[1]]
                    
                    
                if len(args.csv) > 0:
                    with open(args.csv, 'w') as csvfile:
                        dsp("writing results into {} ".format(args.csv))
                        fieldnames = ['', 'Area', 'Mean',
                                      'Min', 'Max', 'X', 'Y']
                        writer = csv.DictWriter(
                            csvfile, fieldnames=fieldnames)
                        writer.writeheader()
                        count = 1
                        for p in term_pos:
                            #if p[2]>= args.prob:
                                #p[0] -= img_offset[0]
                                #p[1] -= img_offset[1]
                                writer.writerow(
                                    {'': str(count), 'X': str((p[1]-img_offset[1])*args.scale), 'Y': str((p[0]-img_offset[0])*args.scale)})
                                count += 1

                if len(args.roi) > 0 and len(term_pos)>0:
                        plist = []
                        for p in term_pos:
                            plist += [[(p[1]-img_offset[1])*args.scale,(p[0]-img_offset[0])*args.scale]]
                        #print(plist)
                        roi = ImagejRoi.frompoints(plist)
                        roi.roitype=ROI_TYPE.POINT
                        #print(roi.coordinates())
                        roi.tofile(args.roi)

                print("MIN {} MAX {}".format(np.min(rimg), np.max(rimg)))
                print("{} {}".format(np.sum(rimg >= 0.5), np.sum(rimg < 0.5)))


                if len(args.dout) > 0:
                     rimg_raw = rimg_raw[img_offset[0]: img_offset[0] + img_fg_.shape[0],
                            img_offset[1]: img_offset[1] + img_fg_.shape[1]]
                     dsp("writing debug image to {}".format(args.dout))
                     scipy.misc.imsave(args.dout, rimg_raw*255)

                if len(args.out) > 0:
                    dsp("writing predictions into the image {}".format(args.out))
                    scipy.misc.imsave(args.out, rimg*255)

                if len(args.cout) > 0:
                    dsp("writing predictions into the image {}".format(args.cout))
                    struct1 = scipy.ndimage.generate_binary_structure(2, 1)
                    struct1 =  morphology.disk(args.terminal_size)
                    rimg = scipy.ndimage.binary_dilation(rimg, structure=struct1, iterations=1).astype(
                        rimg.dtype)

                    imgs = np.zeros(
                        (img_fg.shape[0], img_fg.shape[1], 3), dtype=np.float32)
                    img_fg = img_fg - np.min(img_fg)
                    img_fg = img_fg / (np.max(img_fg)+0.00000001)
                    #imgs[:, :, 1] = (img_fg**4)*255
                    
                    
                    
                    
                    if mycolors is not None:
                        
                        color_img = mycolors[0]
                        color_labels = mycolors[1]
                        color_mask = mycolors[2]
                        
                        if args.cout_dark:
                            img_fg = 1 - img_fg
                            
                        
                    
                        for c in range(0,3):
                            alpha = args.terminal_alpha
                            imgs[:, :, c] =  img_fg * (255 * color_img[c])
                            imgs[:, :, c] = np.multiply((1-rimg), imgs[:, :, c]) + \
                                    (rimg) * 255 * color_labels[c] * (alpha)  +  np.multiply((rimg),imgs[:, :, c] ) * (1-alpha)
                            
                        if img_mask is not None:
                                struct1 = scipy.ndimage.generate_binary_structure(2, 1)
                                img_mask_b = scipy.ndimage.binary_dilation(img_mask, structure=struct1, iterations=5).astype(
                                    rimg.dtype)
                                img_mask_border =  ((img_mask_b - img_mask) > 0.5)
                                img_mask_outer = (np.ones(img_mask.shape)-img_mask_b) > 0.5
                                for c in range(0,3):
                                    imgs[:, :, c]  = np.multiply((1 - img_mask_border) , np.squeeze(imgs[:, :, c])) + 255 * color_mask[c] * img_mask_border
                                    imgs[:, :, c] = np.multiply((1-img_mask_outer), imgs[:, :, c]) + \
                                    (img_mask_outer) * 255 * color_mask[c] * (args.mask_alpha)  +  np.multiply((img_mask_outer),imgs[:, :, c] ) * (1-args.mask_alpha)
                    
                    else:
                        imgs[:, :, 1] = img_fg*255
                        imgs[:, :, 0] = 255*rimg
                        if img_mask is not None:
                            struct1 = scipy.ndimage.generate_binary_structure(2, 1)
                            img_mask_b = scipy.ndimage.binary_dilation(img_mask, structure=struct1, iterations=5).astype(
                                rimg.dtype)
                            imgs[:, :, 2] = (255 * args.mask_alpha)* \
                                (np.ones(img_mask.shape)-img_mask_b) + \
                                200 * (img_mask_b - img_mask)

   
                        # inverting colors
                        if not args.cout_dark:
                            alpha = np.max(imgs, axis=2) / (255.0)
                            imgs = 255 + \
                                np.concatenate([alpha[:, :, np.newaxis], alpha[:, :, np.newaxis], alpha[:, :, np.newaxis]],
                                               axis=2) * (imgs - 255)

                    im_d = Image.fromarray(
                        np.round(imgs).astype(np.uint8), mode='RGB')
                    fn = args.cout
                    fn = fn.replace("tiff", "jpg")
                    print(fn)
                    if "jpg" in fn:
                        im_d.save(fn, "JPEG", quality=95,
                                  optimize=True, progressive=False)
                    else:
                        im_d.save(fn, "PNG")
                        
                
                if False:
                    for cindx in range(0,len(args.myout)):
                        cmd = args.myout[cindx]
                        
                        shape = img_mask.shape
                        
                        img_OUT = np.zeros((img_fg.shape[0], img_fg.shape[1], 3), dtype=np.float32)
                        
                        img_IN = img_fg - np.min(img_fg)
                        img_IN = img_fg / (np.max(img_fg)+0.00000001)
                        
                        struct1 = scipy.ndimage.generate_binary_structure(2, 1)
                        img_TERMINALS = scipy.ndimage.binary_dilation(img_mask, structure=struct1, iterations=args.terminal_size).astype(rimg.dtype)
                        
                        struct1 = scipy.ndimage.generate_binary_structure(2, 1)
                        img_MASK = scipy.ndimage.binary_dilation(img_mask, structure=struct1, iterations=5).astype(
                            rimg.dtype)
                        
                        eval(cmd)
                        
                        if 'R' in locals():
                            img_OUT[:, :, 0] = R
                        if 'G' in locals():
                            img_OUT[:, :, 1] = G
                        if 'B' in locals():
                            img_OUT[:, :, 2] = B
                        
                        im_d = Image.fromarray(
                        np.round(img_OUT).astype(np.uint8), mode='RGB')
                        
                        
                        if "jpg" in fn:
                            im_d.save(fn, "JPEG", quality=95,
                                      optimize=True, progressive=False)
                        else:
                            im_d.save(fn, "PNG")
                        
                            
                    

                dsp("done")


print("gracefully exiting program")
